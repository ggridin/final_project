package com.app.finalProject;

import com.app.finalProject.dao.*;
import com.app.finalProject.entities.*;
import com.app.finalProject.services.UsersServiceImpl;
import org.apache.catalina.User;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
public class FinalProjectApplication {

	@Autowired
	RolesRepository rolesRepository;
	@Autowired
	UsersServiceImpl usersService;
	@Autowired
	PercentsRepository percentsRepository;
	@Autowired
	DateConstantsRepository dateConstantsRepository;
	@Autowired
	ConstantsRepository constantsRepository;

	public static void main(String[] args) {
		SpringApplication.run(FinalProjectApplication.class, args);
	}

	@Bean
	InitializingBean sendDatabase() {
		return () -> {

			List<Percents> percentsList = new ArrayList<>();
			percentsList.add(new Percents("PENALTY",0.01));
			percentsList.add(new Percents("PENALTY_LIMIT",0.40));
			percentsList.add(new Percents("MIN_INTEREST_RATE", 0.05));
			percentsList.add(new Percents("MAX_INTEREST_RATE", 0.1));
			percentsRepository.saveAll(percentsList);

			dateConstantsRepository.save(new DateConstants("MIN_COUNt_OF_YEARS",1L));

			constantsRepository.save(new Constants("MIN_VALUE", 1000.));
			constantsRepository.save(new Constants("MAX_VALUE", 10000.));

			Users user = new Users("1","1","1","1","1","1");
			Users operator = new Users("2","2","2","2","2","2");
			Users admin = new Users("3","3","3","3","3","3");
			Roles roleUser = new Roles(1L,"ROLE_USER");
			Roles roleOperator = new Roles(2L,"ROLE_OPERATOR");
			Roles roleAdmin = new Roles(3L, "ROLE_ADMIN");

			rolesRepository.save(roleUser);
			rolesRepository.save(roleOperator);
			rolesRepository.save(roleAdmin);
			usersService.save(user, roleUser);
			usersService.save(operator, roleOperator);
			usersService.save(admin, roleAdmin);
		};
	}
}
