package com.app.finalProject.services.helpClasses;

import com.app.finalProject.entities.Transfers;
import com.app.finalProject.enums.TransfersStatus;
import org.springframework.stereotype.Service;

@Service
public class BestAntiFraud {
    public static Boolean check(Transfers transfer) {
        if (transfer.getValue().equals(0.)) {
            transfer.setStatus(TransfersStatus.WAITING);
            return false;
        } else {
            transfer.setStatus(TransfersStatus.SUCCESSFUL);
            return true;
        }
    }
}
