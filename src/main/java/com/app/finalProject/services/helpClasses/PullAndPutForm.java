package com.app.finalProject.services.helpClasses;

/*Класс для взаимодействия с пользхователем через веб интерфейс*/
public class PullAndPutForm {
    private Long id;
    private Double money;

    public PullAndPutForm() {

    }

    public PullAndPutForm(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }
}

