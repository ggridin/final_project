package com.app.finalProject.services.helpClasses;

/*Класс для взаимодействия с пользхователем через веб интерфейс*/
public class TransferForm {
    private Long senderId;
    private Long receiverId;
    private Double money;

    public TransferForm(Long senderId) {
        this.senderId = senderId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }
}
