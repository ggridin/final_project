package com.app.finalProject.services;

import com.app.finalProject.entities.CreditLineRequests;
import com.app.finalProject.entities.Users;

import java.util.List;

public interface CreditLineRequestsService {
    public Boolean create(Users user, CreditLineRequests creditLineRequests);
    public void deleteById(Long requestId, Long userId);
    public void update(CreditLineRequests creditLineRequests);
    public List<CreditLineRequests> findAll();
    public CreditLineRequests getOne(Long id);
}
