package com.app.finalProject.services;

import com.app.finalProject.entities.CreditLineRequests;
import com.app.finalProject.entities.CreditLines;

import java.util.List;

public interface CreditLineService {
    public void create(Long userId, Long requestId);
    public void put(Long creditLineId, Double money);
    public void pull(Long creditLineId, Double money);
}
