package com.app.finalProject.services.exceptions;

public class CreditLineServiceException extends RuntimeException {
    public CreditLineServiceException(String message) {
        super(message);
    }
}
