package com.app.finalProject.services.exceptions;

public class TransfersServiceException extends RuntimeException {
    public TransfersServiceException(String message) {
        super(message);
    }
}
