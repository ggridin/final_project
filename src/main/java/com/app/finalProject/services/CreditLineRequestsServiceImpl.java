package com.app.finalProject.services;

import com.app.finalProject.dao.CreditLineRequestsRepository;
import com.app.finalProject.dao.UsersRepository;
import com.app.finalProject.entities.CreditLineRequests;
import com.app.finalProject.entities.Users;
import com.app.finalProject.enums.CreditLineRequestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CreditLineRequestsServiceImpl implements CreditLineRequestsService {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    CreditLineRequestsRepository creditLineRequestsRepository;

    @Override
    @Transactional
    public Boolean create(Users user, CreditLineRequests creditLineRequests) {
        creditLineRequests.setStatus(CreditLineRequestStatus.REQUESTED);
        Users u = usersRepository.getOne(user.getId());
        u.getCreditLineRequests().add(creditLineRequests);
        usersRepository.save(u);
        return true;
    }

    @Override
    @Transactional
    public void deleteById(Long requestId, Long userId) {
        Users user = usersRepository.getOne(userId);
        CreditLineRequests creditLineRequest = creditLineRequestsRepository.getOne(requestId);
        user.getCreditLineRequests().remove(creditLineRequest);
        creditLineRequestsRepository.delete(creditLineRequest);
    }

    @Override
    @Transactional
    public void update(CreditLineRequests creditLineRequests) {
        CreditLineRequests clr = creditLineRequestsRepository.getOne(creditLineRequests.getId());
        clr.setPercent(creditLineRequests.getPercent());
        clr.setValue(creditLineRequests.getValue());
        clr.setStatus(CreditLineRequestStatus.REVIEWED);
        creditLineRequestsRepository.save(clr);
    }

    @Override
    @Transactional
    public List<CreditLineRequests> findAll() {
        return creditLineRequestsRepository.findAll();
    }

    @Override
    @Transactional
    public CreditLineRequests getOne(Long id) {
        return creditLineRequestsRepository.getOne(id);
    }
}
