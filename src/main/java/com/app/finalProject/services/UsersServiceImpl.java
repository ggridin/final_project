package com.app.finalProject.services;

import com.app.finalProject.dao.CreditLineRequestsRepository;
import com.app.finalProject.dao.CreditLinesRepository;
import com.app.finalProject.dao.UsersRepository;
import com.app.finalProject.entities.CreditLineRequests;
import com.app.finalProject.entities.CreditLines;
import com.app.finalProject.entities.Roles;
import com.app.finalProject.entities.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
public class UsersServiceImpl implements UserDetailsService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    CreditLineRequestsRepository creditLineRequestsRepository;

    @Autowired
    CreditLinesRepository creditLinesRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Users user = usersRepository.findByLogin(s);
        if (user == null) throw new UsernameNotFoundException("User not found");
        return user;
    }

    @Transactional
    public Boolean save(Users user, String role) {
        Users userFromDB = usersRepository.findByLogin(user.getLogin());
        if (userFromDB != null) return false;
        user.setRoles(Collections.singleton(new Roles(1L,role)));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        usersRepository.save(user);
        return true;
    }

    @Transactional
    public Boolean save(Users user, Roles role) {
        Users userFromDB = usersRepository.findByLogin(user.getLogin());
        if (userFromDB != null) return false;
        user.setRoles(Collections.singleton(role));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        usersRepository.save(user);
        return true;
    }

    @Transactional
    public List<CreditLineRequests> allCreditLineRequests(Users user) {
        return creditLineRequestsRepository.findAllByUser(user);
    }

    @Transactional
    public List<CreditLines> allCreditLine(Users user) {
        return creditLinesRepository.findAllByUser(user);
    }
}
