package com.app.finalProject.services;

import com.app.finalProject.entities.BankAccounts;

import java.util.List;

public interface BankAccountService {
    public List<BankAccounts> findAll();
    public List<BankAccounts> findAllByUserId(Long userId);
    public BankAccounts getOne(Long id);
    public void create(BankAccounts bankAccounts, Long userId);
    public void pull(Long bankAccountId, Double money);
    public void put(Long bankAccountId, Double money);
    public void close(Long bankAccountId);
}
