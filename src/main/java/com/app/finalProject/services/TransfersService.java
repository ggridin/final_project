package com.app.finalProject.services;

import com.app.finalProject.entities.Transfers;

import java.util.List;

public interface TransfersService {
    public void create(Long accountSenderId, Long accountReceiverId, Double money);
    public List<Transfers> getSentByUserId(Long id);
    public List<Transfers> getReceivedByUserId(Long id);
/*    public void accountToAccountTransfer(Long accountSenderId, Long accountReceiverId, Double money);
    public void accountToCardTransfer(Long accountSenderId, Long cardReceiverId, Double money);
    public void cardToAccountTransfer(Long cardSenderId, Long accountReceiverId, Double money);
    public void cardToCardTransfer(Long cardSenderId, Long cardReceiverId, Double money);*/
}
