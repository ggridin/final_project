package com.app.finalProject.services;

import com.app.finalProject.dao.CreditLineRequestsRepository;
import com.app.finalProject.dao.CreditLinesRepository;
import com.app.finalProject.dao.CreditsRepository;
import com.app.finalProject.dao.UsersRepository;
import com.app.finalProject.entities.CreditLineRequests;
import com.app.finalProject.entities.CreditLines;
import com.app.finalProject.entities.Credits;
import com.app.finalProject.entities.Users;
import com.app.finalProject.enums.CreditLineRequestStatus;
import com.app.finalProject.services.exceptions.CreditLineServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class CreditLineServiceImpl implements CreditLineService {

    @Autowired
    UsersRepository usersRepository;
    @Autowired
    CreditLinesRepository creditLinesRepository;
    @Autowired
    CreditLineRequestsRepository creditLineRequestsRepository;
    @Autowired
    CreditsRepository creditsRepository;

    @Override
    @Transactional
    public void create(Long userId, Long requestId) {
        CreditLineRequests creditLineRequests = creditLineRequestsRepository.getOne(requestId);
        CreditLines creditLine = new CreditLines(creditLineRequests);
        creditLine.setDebt(0.);
        Users u = usersRepository.getOne(userId);
        creditLine.setUser(u);
        u.getCreditLines().add(creditLine);
        u.getCreditLineRequests().remove(creditLineRequests);
        creditLineRequestsRepository.deleteById(requestId);
        usersRepository.save(u);
    }

    @Override
    @Transactional
    public void put(Long creditLineId, Double money){
        CreditLines creditLine = creditLinesRepository.getOne(creditLineId);
        List<Credits> credits = creditLine.getCredits();
        List<Credits> removed = new ArrayList<>();
        credits.sort(Comparator.comparing(Credits::getEndDate));
        for (Credits credit : credits) {
            if (money >= credit.getDebt()) {
                money -= credit.getDebt();
                removed.add(credit);
                creditLine.setDebt(creditLine.getDebt() - credit.getValue());
                continue;
            }
            credit.setDebt(credit.getDebt() - money);
            break;
        }
        credits.removeAll(removed);
        creditLine.setCredits(credits);
        creditsRepository.deleteAll(removed);
        creditLinesRepository.save(creditLine);
    }

    @Override
    @Transactional
    public void pull(Long creditLineId, Double money)throws CreditLineServiceException {
        CreditLines creditLine = creditLinesRepository.getOne(creditLineId);
        if (money > creditLine.getValue() - creditLine.getDebt()) {
            throw new CreditLineServiceException("Недостаточно средств");
        }

        LocalDate currentDate = LocalDate.now();

        creditLine.setDebt(creditLine.getDebt() + money);
        Credits credit = new Credits(money, money + money * creditLine.getPercent(), currentDate.plusDays(30L),creditLine);
        creditLine.getCredits().add(credit);
        creditLinesRepository.save(creditLine);
    }
}
