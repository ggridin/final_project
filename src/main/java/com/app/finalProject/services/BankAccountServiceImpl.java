package com.app.finalProject.services;

import com.app.finalProject.dao.*;
import com.app.finalProject.entities.*;
import com.app.finalProject.services.exceptions.BankAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    BankAccountsRepository bankAccountsRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    PercentsRepository percentsRepository;
    @Autowired
    ConstantsRepository constantsRepository;
    @Autowired
    DateConstantsRepository dateConstantsRepository;

    @Override
    @Transactional
    public List<BankAccounts> findAll() {
        return bankAccountsRepository.findAll();
    }

    @Override
    @Transactional
    public List<BankAccounts> findAllByUserId(Long userId) {
        Users user = usersRepository.getOne(userId);
        return bankAccountsRepository.findAllByUser(user);
    }

    @Override
    public BankAccounts getOne(Long id) {
        return bankAccountsRepository.getOne(id);
    }

    @Override
    @Transactional
    public void create(BankAccounts bankAccounts, Long userId) throws BankAccountException {
        Users user = usersRepository.getOne(userId);
        Double minInterestRate = percentsRepository.getOne("MIN_INTEREST_RATE").getValue();
        Double maxInterestRate = percentsRepository.getOne("MAX_INTEREST_RATE").getValue();
        Double minValue = constantsRepository.getOne("MIN_VALUE").getValue();
        Double maxValue = constantsRepository.getOne("MAX_VALUE").getValue();
        Long minTime = dateConstantsRepository.getOne("MIN_COUNt_OF_YEARS").getDate();

        if (bankAccounts.getPercent() < minInterestRate || bankAccounts.getPercent() > maxInterestRate) {
            throw new BankAccountException("Недопустимый процент по ставке");
        }
        if (bankAccounts.getValue() < minValue || bankAccounts.getValue() > maxValue) {
            throw new BankAccountException("Недопустимый размер вклада");
        }
        if (ChronoUnit.YEARS.between(bankAccounts.getBeginDate(), bankAccounts.getEndDate()) < minTime) {
            throw new BankAccountException("Недопустимый срок вклада");
        }

        bankAccounts.setUser(user);
        bankAccounts.setMinValueInCurrMonth(bankAccounts.getValue());
        user.getBankAccounts().add(bankAccounts);
        usersRepository.save(user);
    }

    @Override
    @Transactional
    public void pull(Long bankAccountId, Double money) throws BankAccountException {
        BankAccounts bankAccount = bankAccountsRepository.getOne(bankAccountId);
        Double resultValue = bankAccount.getValue() - money;
        if (!bankAccount.isWithdrawalable()) {
            throw new BankAccountException("С этого счета нельзя снимать деньги");
        }
        if (resultValue < 0) {
            throw new BankAccountException("Недостаточно дженег на счете");
        }
        bankAccount.setValue(resultValue);
        if (resultValue < bankAccount.getMinValueInCurrMonth()) {
            bankAccount.setMinValueInCurrMonth(resultValue);
        }
        //bankAccountsRepository.save(bankAccount);
    }

    @Override
    @Transactional
    public void put(Long bankAccountId, Double money) throws BankAccountException {
        BankAccounts bankAccount = bankAccountsRepository.getOne(bankAccountId);
        if (!bankAccount.isReplenishable()) {
            throw new BankAccountException("На этот счет нельзя класть деньги");
        }
        bankAccount.setValue(bankAccount.getValue() + money);
        //bankAccountsRepository.save(bankAccount);
    }

    @Override
    @Transactional
    public void close(Long bankAccountId) {
        BankAccounts bankAccount = bankAccountsRepository.getOne(bankAccountId);
        if (!bankAccount.isLockable()) {
            throw new BankAccountException("Этот счет нельзя закрыть");
        }
        Users user = bankAccount.getUser();
        user.getBankAccounts().remove(bankAccount);
        usersRepository.save(user);
        bankAccountsRepository.delete(bankAccount);
    }

    @Scheduled(cron="0 0 1 1 1/1 ? *")
    @Transactional
    public void update() {
        List<BankAccounts> bankAccounts = bankAccountsRepository.findAll();
        bankAccounts.parallelStream().forEach((account) -> {
            if(account.isCapitalizable()) {
                Double result = account.getMinValueInCurrMonth() * account.getPercent() / 12;
                account.setValue(account.getValue() + result);
            }
        });
        bankAccountsRepository.saveAll(bankAccounts);
    }

    @Scheduled(cron="0 0 1 1 1/1 ? *")
    @Transactional
    public void monthlyUpdate() {
        List<BankAccounts> bankAccounts = bankAccountsRepository.findAll();
        bankAccounts.parallelStream().forEach((account) -> {
            if(account.isCapitalizable()) {
                Double result = account.getMinValueInCurrMonth() * account.getPercent() / 12;
                account.setValue(account.getValue() + result);
            }
        });
        bankAccountsRepository.saveAll(bankAccounts);
    }

    @Scheduled(cron="0 0 12 1 1 ? *")
    @Transactional
    public void yearlyUpdate() {
        List<BankAccounts> bankAccounts = bankAccountsRepository.findAll();
        bankAccounts.parallelStream().forEach((account) -> {
            if(!account.isCapitalizable()) {
                Double result = account.getMinValueInCurrMonth() * account.getPercent() / 12;
                account.setValue(account.getValue() + result);
            }
        });
        bankAccountsRepository.saveAll(bankAccounts);
    }
}
