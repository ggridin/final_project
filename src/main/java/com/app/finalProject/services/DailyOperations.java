package com.app.finalProject.services;

import com.app.finalProject.dao.CreditsRepository;
import com.app.finalProject.dao.CurrencyRepository;
import com.app.finalProject.dao.PercentsRepository;
import com.app.finalProject.entities.Credits;
import com.app.finalProject.entities.Currency;
import com.app.finalProject.entities.Percents;
import com.app.finalProject.services.helpClasses.JsonReader;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;


@Service
public class DailyOperations {

    @Autowired
    CreditsRepository creditsRepository;
    @Autowired
    PercentsRepository percentsRepository;
    @Autowired
    CurrencyRepository currencyRepository;

    @Transactional
    void updateCurrency() throws IOException, JSONException {
        JSONObject jsonObject = JsonReader.readJsonFromUrl("https://www.cbr-xml-daily.ru/daily_json.js");
        Double usd = Double.valueOf(jsonObject.getJSONObject("Valute").getJSONObject("USD").getString("Value"));
        Double eur = Double.valueOf(jsonObject.getJSONObject("Valute").getJSONObject("EUR").getString("Value"));
        currencyRepository.save(new Currency("USD", usd));
        currencyRepository.save(new Currency("EUR", eur));
    }

    @Transactional
    void penalty() {
        List<Credits> credits = creditsRepository.findAll();
        Percents penalty = percentsRepository.getOne("PENALTY");
        Percents penaltyLimit = percentsRepository.getOne("PENALTY_LIMIT");
        LocalDate currentDate = LocalDate.now();

        credits.parallelStream().forEach((credit) -> {
            if (credit.getEndDate().isBefore(currentDate)) {
                double currentPenalty = credit.getDebt() * penalty.getValue();
                if(credit.getPenalty() + currentPenalty < credit.getValue() * penaltyLimit.getValue()) {
                    credit.setPenalty(credit.getPenalty()+currentPenalty);
                    credit.setDebt(credit.getDebt() + currentPenalty);
                }
            }
        });
        creditsRepository.saveAll(credits);
    }


    /* Не знаю как обработать случай если ЦБ не отвечает*/
    @Scheduled(cron = "0 1 * * ?")
    public void update() {
        penalty();
        try {
            updateCurrency();
        } catch (JSONException e) {

        }
        catch (IOException e) {

        }
    }
}
