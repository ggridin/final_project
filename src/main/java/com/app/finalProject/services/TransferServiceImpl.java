package com.app.finalProject.services;

import com.app.finalProject.dao.BankAccountsRepository;
import com.app.finalProject.dao.TransfersRepository;
import com.app.finalProject.entities.BankAccounts;
import com.app.finalProject.entities.Transfers;
import com.app.finalProject.services.exceptions.BankAccountException;
import com.app.finalProject.services.exceptions.TransfersServiceException;
import com.app.finalProject.services.helpClasses.BestAntiFraud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransferServiceImpl implements TransfersService {
    @Autowired
    BankAccountService bankAccountService;
    @Autowired
    TransfersRepository transfersRepository;

    @Override
    @Transactional
    public void create(Long accountSenderId, Long accountReceiverId, Double money) throws BankAccountException, TransfersServiceException{
        BankAccounts receiver = bankAccountService.getOne(accountReceiverId);
        BankAccounts sender = bankAccountService.getOne(accountSenderId);

        Transfers transfer = new Transfers(LocalDateTime.now(), sender, receiver);
        transfer.setValue(money);
        receiver.getReceived().add(transfer);
        sender.getSent().add(transfer);

        if (BestAntiFraud.check(transfer)) {
            bankAccountService.put(accountReceiverId, money);
            bankAccountService.pull(accountSenderId, money);
        } else {
            throw new TransfersServiceException("Трансфер заблокирован и ожидает рассмотрения");
        }
    }

    @Override
    @Transactional
    public List<Transfers> getSentByUserId(Long id) {
        return transfersRepository.findAllBySenderId(id);
    }

    @Override
    @Transactional
    public List<Transfers> getReceivedByUserId(Long id) {
        return transfersRepository.findAllByReceiverId(id);
    }
}
