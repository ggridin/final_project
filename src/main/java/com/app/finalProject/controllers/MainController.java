package com.app.finalProject.controllers;

import com.app.finalProject.dao.CreditLineRequestsRepository;
import com.app.finalProject.entities.*;
import com.app.finalProject.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    UsersServiceImpl usersService;
    @Autowired
    CreditLineRequestsService creditLineRequestsService;
    @Autowired
    CreditLineService creditLineService;
    @Autowired
    BankAccountService bankAccountService;
    @Autowired
    TransfersService transfersService;

    @GetMapping(value = {"/"})
    public String index() {
        return "index";
    }

    @GetMapping(value = {"/user"})
    public String user(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Users user = (Users) principal;
        List<CreditLineRequests> creditLineRequestsList = usersService.allCreditLineRequests(user);
        List<CreditLines> creditLines = usersService.allCreditLine(user);
        List<BankAccounts> bankAccounts = bankAccountService.findAllByUserId(user.getId());
        List<Transfers> sent = transfersService.getSentByUserId(user.getId());
        List<Transfers> received = transfersService.getReceivedByUserId(user.getId());
        model.addAttribute("requests", creditLineRequestsList);
        model.addAttribute("creditLines", creditLines);
        model.addAttribute("bankAccounts", bankAccounts);
        model.addAttribute("sent", sent);
        model.addAttribute("received", received);
        return "user";
    }

    @GetMapping(value = {"/operator"})
    public String operator(Model model) {
        model.addAttribute("requests", creditLineRequestsService.findAll());
        return "operator";
    }
}
