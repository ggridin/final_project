package com.app.finalProject.controllers;

import com.app.finalProject.entities.*;
import com.app.finalProject.services.*;
import com.app.finalProject.services.exceptions.BankAccountException;
import com.app.finalProject.services.exceptions.CreditLineServiceException;
import com.app.finalProject.services.exceptions.TransfersServiceException;
import com.app.finalProject.services.helpClasses.PullAndPutForm;
import com.app.finalProject.services.helpClasses.TransferForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

    @Autowired
    CreditLineRequestsService creditLineRequestsService;
    @Autowired
    CreditLineService creditLineService;
    @Autowired
    BankAccountService bankAccountService;
    @Autowired
    TransfersService transfersService;

    @GetMapping(value = "/user/createCreditLineRequests")
    public String createCreditLineRequests(Model model) {
        CreditLineRequests creditLineRequests = new CreditLineRequests();
        model.addAttribute("createForm", creditLineRequests);
        return "createCreditLineRequest";
    }

    @PostMapping(value = "/user/createCreditLineRequests")
    public String createCreditLineRequest(@ModelAttribute("createForm") CreditLineRequests createForm, BindingResult bindingResult, Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Users user = (Users) principal;
        creditLineRequestsService.create(user, createForm);
        return "redirect:/user";
    }

    @GetMapping(value="/user/close")
    public String cancelCreditLineRequest(@RequestParam(name = "id") String id) {
        Long requestId = Long.valueOf(id);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Users user = (Users) principal;
        creditLineRequestsService.deleteById(requestId, user.getId());
        return "redirect:/user";
    }

    @GetMapping(value="/user/accept")
    public String acceptCreditLineRequest(@RequestParam(name = "id") String id, Model model) {
        Long requestId = Long.valueOf(id);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Users user = (Users) principal;
        creditLineService.create(user.getId(), requestId);
        return "redirect:/user";
    }

    @GetMapping(value = "/user/putCreditLine")
    public String putCreditLinePage(@RequestParam(name="creditLineId") String id, Model model) {
        Long creditLineId = Long.valueOf(id);
        PullAndPutForm put = new PullAndPutForm(creditLineId);
        model.addAttribute("form", put);
        return "putCreditLinePage";
    }

    @PostMapping(value = "/user/putCreditLine")
    public String putCreditLine(@ModelAttribute("form") PullAndPutForm put, Model model) {
        creditLineService.put(put.getId(), put.getMoney());
        return "redirect:/user";
    }

    @GetMapping(value = "/user/pullCreditLine")
    public String pullCreditLinePage(@RequestParam(name="creditLineId") String id, Model model) {
        Long creditLineId = Long.valueOf(id);
        PullAndPutForm pull = new PullAndPutForm(creditLineId);
        model.addAttribute("form", pull);
        return "pullCreditLinePage";
    }

    @PostMapping(value = "/user/pullCreditLine")
    public String pullCreditLine(@ModelAttribute("form") PullAndPutForm pull, Model model) {
        try {
            creditLineService.pull(pull.getId(), pull.getMoney());
        }
        catch (CreditLineServiceException e) {
            model.addAttribute("message", e.getMessage());
            return "pullCreditLinePage";
        }
        return "redirect:/user";
    }

    @GetMapping(value = "/user/createBankAccount")
    public String createBankAccountPage(Model model) {
        BankAccounts bankAccounts = new BankAccounts();
        model.addAttribute("bankAccount", bankAccounts);
        return "createBankAccount";
    }

    @PostMapping(value="/user/createBankAccount")
    public String createBankAccount(@ModelAttribute("bankAccount") BankAccounts bankAccount, Model model) {
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            Users user = (Users) principal;
            bankAccountService.create(bankAccount, user.getId());
            return "redirect:/user";
        } catch (BankAccountException e) {
            model.addAttribute("message", e.getMessage());
            return "createBankAccount";
        }
    }

    @GetMapping(value = "/user/closeBankAccount")
    public String closeBankAccount(@RequestParam(name="id") String id, Model model) {
        try {
            bankAccountService.close(Long.valueOf(id));
            return "redirect:/user";
        } catch (BankAccountException e) {
            model.addAttribute("message", e.getMessage());
            return "redirect:/user";
        }
    }

    @GetMapping(value="/user/put")
    public String putBankAccountPage(@RequestParam(name="id") String id, Model model) {
        PullAndPutForm put = new PullAndPutForm(Long.valueOf(id));
        model.addAttribute("form", put);
        return "putPage";
    }

    @PostMapping(value="/user/put")
    public String putBankAccount(@ModelAttribute("form") PullAndPutForm put, Model model) {
        try {
            bankAccountService.put(put.getId(), put.getMoney());
            return "redirect:/user";
        } catch (BankAccountException e) {
            model.addAttribute("message", e.getMessage());
            return "putPage";
        }
    }

    @GetMapping(value="/user/pull")
    public String pullBankAccountPage(@RequestParam(name="id") String id, Model model) {
        PullAndPutForm pull = new PullAndPutForm(Long.valueOf(id));
        model.addAttribute("form", pull);
        return "pullPage";
    }

    @PostMapping(value="/user/pull")
    public String pullBankAccount(@ModelAttribute("form") PullAndPutForm pull, Model model) {
        try {
            bankAccountService.pull(pull.getId(), pull.getMoney());
            return "redirect:/user";
        } catch (BankAccountException e) {
            model.addAttribute("message", e.getMessage());
            return "pullPage";
        }
    }

    @GetMapping(value = "/user/transfer")
    public String transferPage(@RequestParam(name="id") String id, Model model) {
        model.addAttribute("form", new TransferForm(Long.valueOf(id)));
        return "transfer";
    }

    @PostMapping(value = "/user/transfer")
    public String transfer(@ModelAttribute("form") TransferForm form, Model model) {
        try {
            transfersService.create(form.getSenderId(), form.getReceiverId(), form.getMoney());
            return "redirect:/user";
        } catch (TransfersServiceException | BankAccountException e) {
            model.addAttribute("message", e.getMessage());
            return "transfer";
        }
    }
}
