package com.app.finalProject.controllers;

import com.app.finalProject.entities.CreditLineRequests;
import com.app.finalProject.services.CreditLineRequestsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class OperatorController {

    @Autowired
    CreditLineRequestsService creditLineRequestsService;

    @RequestMapping(value="/operator/editCreditLineRequest", method = RequestMethod.GET)
    public String getEditPage(@RequestParam(name = "id") String id, Model model) {
        CreditLineRequests request = creditLineRequestsService.getOne(Long.valueOf(id));
        model.addAttribute("request", request);
        return "editCreditLineRequest";
    }

    @RequestMapping(value="/operator/editCreditLineRequest", method = RequestMethod.POST)
    public String update(@ModelAttribute("request") CreditLineRequests request, Model model) {
        creditLineRequestsService.update(request);
        return "redirect:/operator";
    }
}
