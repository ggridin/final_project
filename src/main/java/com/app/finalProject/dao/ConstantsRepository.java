package com.app.finalProject.dao;

import com.app.finalProject.entities.Constants;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConstantsRepository extends JpaRepository<Constants, String> {



}
