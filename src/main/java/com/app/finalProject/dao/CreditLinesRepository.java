package com.app.finalProject.dao;

import com.app.finalProject.entities.CreditLines;
import com.app.finalProject.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CreditLinesRepository extends JpaRepository<CreditLines, Long> {
    public List<CreditLines> findAllByUser(Users user);
}
