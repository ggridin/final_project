package com.app.finalProject.dao;

import com.app.finalProject.entities.Credits;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CreditsRepository extends JpaRepository<Credits, Long> {
    public List<Credits> findAll();
}
