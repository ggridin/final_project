package com.app.finalProject.dao;

import com.app.finalProject.entities.CreditLineRequests;
import com.app.finalProject.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CreditLineRequestsRepository extends JpaRepository<CreditLineRequests, Long> {
    List<CreditLineRequests> findAllByUser(Users user);
}
