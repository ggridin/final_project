package com.app.finalProject.dao;

import com.app.finalProject.entities.DateConstants;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DateConstantsRepository extends JpaRepository<DateConstants, String> {

}
