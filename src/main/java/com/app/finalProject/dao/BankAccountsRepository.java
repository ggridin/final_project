package com.app.finalProject.dao;

import com.app.finalProject.entities.BankAccounts;
import com.app.finalProject.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BankAccountsRepository extends JpaRepository<BankAccounts, Long> {
    public BankAccounts getById(Long id);
    public List<BankAccounts> findAllByUser(Users user);
}
