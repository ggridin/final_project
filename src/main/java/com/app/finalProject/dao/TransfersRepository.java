package com.app.finalProject.dao;

import com.app.finalProject.entities.Transfers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransfersRepository extends JpaRepository<Transfers, Long> {
    public List<Transfers> findAllByReceiverId(Long id);
    public List<Transfers> findAllBySenderId(Long id);
}
