package com.app.finalProject.dao;

import com.app.finalProject.entities.Percents;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PercentsRepository extends JpaRepository<Percents, String> {

}
