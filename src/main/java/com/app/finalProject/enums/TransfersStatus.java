package com.app.finalProject.enums;

public enum TransfersStatus {
    WAITING,
    SUCCESSFUL;
}
