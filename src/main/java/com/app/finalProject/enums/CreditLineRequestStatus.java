package com.app.finalProject.enums;

public enum CreditLineRequestStatus {
    REQUESTED,
    REVIEWED,
}
