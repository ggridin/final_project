package com.app.finalProject.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@SequenceGenerator(name="seq", initialValue=1)
public class Credits {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;

    @Column
    private double value;

    @Column
    private double debt;

    @Column
    private double penalty;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ManyToOne
    private CreditLines creditLines;

    public Credits(double value, double debt, LocalDate endDate, CreditLines creditLines) {
        this.value = value;
        this.debt = debt;
        this.endDate = endDate;
        this.creditLines = creditLines;
    }

    public Credits() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getDebt() {
        return debt;
    }

    public void setDebt(double debt) {
        this.debt = debt;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public CreditLines getCreditLines() {
        return creditLines;
    }

    public void setCreditLines(CreditLines creditLines) {
        this.creditLines = creditLines;
    }

    public double getPenalty() {
        return penalty;
    }

    public void setPenalty(double penalty) {
        this.penalty = penalty;
    }
}
