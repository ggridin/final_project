package com.app.finalProject.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class DateConstants {
    @Id
    private String name;

    @Column
    private Long count;

    public DateConstants() {

    }

    public DateConstants(String name, Long date) {
        this.name = name;
        this.count = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDate() {
        return count;
    }

    public void setDate(Long count) {
        this.count = count;
    }
}
