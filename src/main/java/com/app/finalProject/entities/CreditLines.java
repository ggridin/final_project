package com.app.finalProject.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@SequenceGenerator(name="seq", initialValue=1)
public class CreditLines {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;
    @Column
    private Double value;
    @Column
    private Double percent;
    @Column
    private Double debt;
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginDate;
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Credits> credits;

    @ManyToOne
    private Users user;

    public CreditLines() {

    }

    public CreditLines(CreditLineRequests creditLineRequests) {
        this.beginDate = creditLineRequests.getBeginDate();
        this.endDate = creditLineRequests.getEndDate();
        this.value = creditLineRequests.getValue();
        this.percent = creditLineRequests.getPercent();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public List<Credits> getCredits() {
        return credits;
    }

    public void setCredits(List<Credits> credits) {
        this.credits = credits;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Double getDebt() {
        return debt;
    }

    public void setDebt(Double debt) {
        this.debt = debt;
    }
}

