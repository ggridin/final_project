package com.app.finalProject.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Set;

@Entity
@SequenceGenerator(name="seq", initialValue=1)
public class Users implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;
    @Column
    private String firstName;
    @Column
    private String secondName;
    @Column
    private String thirdName;
    @Column
    private String login;
    @Column
    private String password;

    @Transient
    private String passwordConfirm;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn
    private Set<Roles> roles;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    private Set<CreditLineRequests> creditLineRequests;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    private Set<CreditLines> creditLines;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    private Set<BankAccounts> bankAccounts;

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public void setRoles(Set<Roles> roles) {
        this.roles = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Users() {

    }

    public Users( String firstName, String secondName, String thirdName, String login, String password, String passwordConfirm) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.thirdName = thirdName;
        this.login = login;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getThirdName() {
        return thirdName;
    }

    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Roles> getRoles() {
        return roles;
    }

    public void setRole(Set<Roles> role) {
        this.roles = roles;
    }

    public Set<CreditLineRequests> getCreditLineRequests() {
        return creditLineRequests;
    }

    public void setCreditLineRequests(Set<CreditLineRequests> creditLineRequests) {
        this.creditLineRequests = creditLineRequests;
    }

    public Set<CreditLines> getCreditLines() {
        return creditLines;
    }

    public void setCreditLines(Set<CreditLines> creditLines) {
        this.creditLines = creditLines;
    }

    public Set<BankAccounts> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(Set<BankAccounts> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }
}
