package com.app.finalProject.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@SequenceGenerator(name="seq", initialValue=1)
public class BankAccounts {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;

    @Column
    private double value;

    @Column
    private Double minValueInCurrMonth;

    @OneToMany
    private Set<CreditCards> creditCards;

    @OneToMany(mappedBy = "sender")
    private Set<Transfers> sent;

    @OneToMany(mappedBy = "receiver")
    private Set<Transfers> received;

    @ManyToOne
    private Users user;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginDate;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @Column
    private Boolean replenishable;

    @Column
    private Boolean lockable;

    @Column
    private Boolean capitalizable;

    @Column
    private Boolean withdrawalable;

    @Column
    private Double percent;

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Boolean getReplenishable() {
        return replenishable;
    }

    public Boolean getLockable() {
        return lockable;
    }

    public Boolean getCapitalizable() {
        return capitalizable;
    }

    public Boolean getWithdrawalable() {
        return withdrawalable;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Boolean isReplenishable() {
        return replenishable;
    }

    public void setReplenishable(Boolean replenishable) {
        this.replenishable = replenishable;
    }

    public Boolean isLockable() {
        return lockable;
    }

    public void setLockable(Boolean lockable) {
        this.lockable = lockable;
    }

    public Boolean isCapitalizable() {
        return capitalizable;
    }

    public void setCapitalizable(Boolean capitalizable) {
        this.capitalizable = capitalizable;
    }

    public Boolean isWithdrawalable() {
        return withdrawalable;
    }

    public void setWithdrawalable(Boolean withdrawalable) {
        this.withdrawalable = withdrawalable;
    }

    public Long getId() {
        return id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Set<CreditCards> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(Set<CreditCards> creditCards) {
        this.creditCards = creditCards;
    }

    public Set<Transfers> getSent() {
        return sent;
    }

    public void setSent(Set<Transfers> sent) {
        this.sent = sent;
    }

    public Set<Transfers> getReceived() {
        return received;
    }

    public void setReceived(Set<Transfers> received) {
        this.received = received;
    }

    public Double getMinValueInCurrMonth() {
        return minValueInCurrMonth;
    }

    public void setMinValueInCurrMonth(Double minValueInCurrMonth) {
        this.minValueInCurrMonth = minValueInCurrMonth;
    }

}
