package com.app.finalProject.entities;

import com.app.finalProject.enums.CreditLineRequestStatus;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;


@Entity
@SequenceGenerator(name="seq", initialValue=1)
public class CreditLineRequests {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;
    @Column
    private Double percent;
    @Column
    private Double value;
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginDate;
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ManyToMany(mappedBy = "creditLineRequests")
    private Set<Users> user;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private CreditLineRequestStatus status;

    public CreditLineRequests() {

    }

    public CreditLineRequests(LocalDate date, CreditLineRequestStatus status) {
        this.beginDate = date;
        this.status = status;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Set<Users> getUser() {
        return user;
    }

    public void setUser(Set<Users> user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public CreditLineRequestStatus getStatus() {
        return status;
    }

    public void setStatus(CreditLineRequestStatus status) {
        this.status = status;
    }
}
