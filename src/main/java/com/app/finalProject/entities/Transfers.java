package com.app.finalProject.entities;

import com.app.finalProject.enums.CreditLineRequestStatus;
import com.app.finalProject.enums.TransfersStatus;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@SequenceGenerator(name="seq", initialValue=1)
public class Transfers {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;

    @Column
    private LocalDateTime time;

    @Column
    private Double value;

    @ManyToOne
    private BankAccounts sender;

    @ManyToOne
    private BankAccounts receiver;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private TransfersStatus status;

    public Transfers() {

    }

    public Transfers(LocalDateTime time, BankAccounts sender, BankAccounts receiver) {
        this.time = time;
        this.sender = sender;
        this.receiver = receiver;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public BankAccounts getSender() {
        return sender;
    }

    public void setSender(BankAccounts sender) {
        this.sender = sender;
    }

    public BankAccounts getReceiver() {
        return receiver;
    }

    public void setReceiver(BankAccounts receiver) {
        this.receiver = receiver;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public TransfersStatus getStatus() {
        return status;
    }

    public void setStatus(TransfersStatus status) {
        this.status = status;
    }
}
